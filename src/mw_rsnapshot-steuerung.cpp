// mw 9.8.2009
// Steuerung für rsnapshot

#include <vector>
#include <iostream>
#include <string>
#include <time.h>
#include <sys/stat.h>
#include <stdlib.h>

struct EintragTyp {
    std::string Dateiname;
    int Ablaufintervall;
    std::string Aktion;
    EintragTyp( std::string const & a_Dateiname, int a_Ablaufintervall, std::string const & a_Aktion )
	: Dateiname( a_Dateiname ), Ablaufintervall( a_Ablaufintervall ), Aktion( a_Aktion ) {}
};

typedef std::vector< EintragTyp > EintragListe;

int main()
{
    EintragListe t_Liste;
    t_Liste.push_back( EintragTyp( "/var/sambadata/snapshots/hourly.0", 60*60, "/usr/bin/rsnapshot hourly" ) );
    t_Liste.push_back( EintragTyp( "/var/sambadata/snapshots/daily.0", 60*60*24, "/usr/bin/rsnapshot daily" ) );
    t_Liste.push_back( EintragTyp( "/var/sambadata/snapshots/weekly.0", 60*60*24*7, "/usr/bin/rsnapshot weekly" ) );
    t_Liste.push_back( EintragTyp( "/var/sambadata/snapshots/monthly.0", 60*60*24*30, "/usr/bin/rsnapshot monthly" ) );
    t_Liste.push_back( EintragTyp( "/var/sambadata/snapshots/yearly.0", 60*60*24*365, "/usr/bin/rsnapshot yearly" ) );

    std::vector<std::string> t_GeplanteAktionen;

    for ( EintragListe::iterator t_Iter = t_Liste.begin(); t_Iter != t_Liste.end(); ++t_Iter )
    {
        EintragTyp const & t_AktEintrag = *t_Iter;
        std::cout << "Prüfe " << t_AktEintrag.Dateiname << std::endl;

        struct stat statbuf;
        if ( stat( t_AktEintrag.Dateiname.c_str(), &statbuf ) )
        {
            std::cerr << "Fehler bei stat auf " << t_AktEintrag.Dateiname << std::endl;
            continue;
        }
        time_t t_ctime = statbuf.st_ctime;
        time_t t_Jetzt = time(0);
        int t_Differenz = difftime( t_Jetzt, t_ctime );
        
        std::cout << "Aktuelle Differenz: " << t_Differenz << " Grenzwert: " << t_AktEintrag.Ablaufintervall << std::endl;
        
        // mw 29.5.2024 10 Sekunden Toleranz nach unten zulassen
        if ( t_Differenz > t_AktEintrag.Ablaufintervall - 10 )
        {
            std::cout << "Aktion planen: " << t_AktEintrag.Aktion << std::endl;
            t_GeplanteAktionen.push_back(t_AktEintrag.Aktion);
        }
        else
        {
            // mw 29.5.2024: Wenn in der aktuellen Pruefung keine Aktion geplant wurde, gar nicht weiter pruefen
            std::cout << "Keine weiteren Bedingungen pruefen.\n";
            break;
        }
    }

    if ( t_GeplanteAktionen.size() > 0 )
    {
        for( auto const & t_Akt : t_GeplanteAktionen)
        {
            std::cout << "Ausfuehren von " << t_Akt << std::endl;
            system( t_Akt.c_str() );
            std::cout << "Aktion abgeschlossen: " << t_Akt << std::endl;

        }
    }
    else
    {
        std::cout << "Aktuell keine Aktionen auszufuehren." << std::endl;
    }

    system( "/bin/df -h" );

    return 0;
}
