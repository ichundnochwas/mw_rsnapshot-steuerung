message("Testmessage aus mw-cppcheck")

add_custom_target(build-cppcheck
    COMMENT "Build cppcheck..."
    COMMAND ${CMAKE_COMMAND} -B cppcheck -S ${CMAKE_SOURCE_DIR}/extern/cppcheck
    COMMAND ${CMAKE_COMMAND} --build cppcheck -j
)

file(GLOB FILES_FOR_CPPCHECK src/*.cpp)

add_custom_target(run-cppcheck
    COMMENT "Run cppcheck..."
    COMMAND cppcheck/bin/cppcheck --check-level=exhaustive --enable=all --output-file=cppcheck-output.txt ${FILES_FOR_CPPCHECK}
)

add_dependencies(run-cppcheck build-cppcheck)

